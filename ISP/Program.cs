﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP
{

    public interface IAnimal
    {
        void Name();
    }
    public interface IOperationAdd
    {
        void AnimalAdd();
    }

    public class AnimalOwner : IOperationAdd
    {
        public void AnimalAdd()
        {
            Console.WriteLine("Added New Animal");
        }
    }

    public class Dog : IAnimal
    {
        public void Name()
        {
            Console.WriteLine("Dog");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
