﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dp_hw7
{

    public interface IAnimal
    {
        void Voice();
    }

    public class Dog : IAnimal
    {
        public void Voice()
        {
            Console.WriteLine("Bark-Bark");
        }
    }

    public class Cat : IAnimal
    {
        public void Voice()
        {
            Console.WriteLine("Meow");
        }

    }

    public class VoiceSynthesizer
    {
        private IAnimal _animal;
        public VoiceSynthesizer(IAnimal animal = null)
        {
            _animal = animal;
        }

        public void DoSynthesizer()
        {
            if(_animal!=null) _animal.Voice();
            else Console.WriteLine("Not an Animal");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var dog = new Dog();
            var synthesizer = new VoiceSynthesizer(dog);
            synthesizer.DoSynthesizer();
            Console.ReadLine();
        }
    }
}
